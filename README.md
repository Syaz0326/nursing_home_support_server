# 介護情報入力システム(サーバサイド)

## 概要
卒業研究で作成する介護情報入力システムのサーバ，データベースおよび管理アプリケーションに関するドキュメント

## 使用ライブラリ・フレームワーク
* [Node.js](https://nodejs.org/ja/)
* [Electron](https://electronjs.org/)
* [SQL.js](https://github.com/kripken/sql.js/)
