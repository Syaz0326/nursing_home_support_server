// 読み込み
const {app, BrowserWindow, ipcMain, dialog} = require('electron')
const path = require("path")
const url = require("url")
const moment = require("moment")
const getIp = require("./server/get_ip.js")

const Store = require("./store/store.js")
let store = new Store()

const ServerMethods = require('./server/server.js')
store.setServer(new ServerMethods())
let server = store.server

let db = store.db

// メインウィンドウ
let win = null

// 保存ダイアログオプション
let dialogOption = {
  title: 'タイトル',
  defaultPath: './backup/' + moment().format('YYYYMMDDHmm'),
  filters: [{
    name: 'sqlite',
    extensions: ['sqlite']
  }]
}

// ウィンドウの生成
let createWindow = () => {
  win = new BrowserWindow({
    width: 800,
    height: 600
  })

  win.loadURL(url.format({
    pathname: path.join(__dirname, 'index.html'),
    protocol: 'file',
    slashes: true
  }))

  win.on('closed', () => {
    win = null
  })
}

// アプリ起動時にメインウィンドウを作成
app.on('ready', createWindow)

// すべてのウィンドウが閉じられたときの動作
app.on('windows-all-closed', () => {
  if(process.platform !== 'darwin') {
    app.quit()
  }
})

// アプリがアクティブになったときの動作(macのみ)
app.on('activate', () => {
  win === null ? createWindow() : ''
  if(win === null) {
    creatWindow()
  }
})

ipcMain.on('run_server', (e) => {
  let HOST = getIp()
  server.start(getIp())
  e.returnValue = {
    HOST: HOST,
    PORT: server.server.address().port
  }
})

ipcMain.on('stop_server', (e) => {
  server.server.close(() => {
    e.returnValue = 'server is stopped'
  })
})

ipcMain.on('register_resident', (e, data) => {
  data.created_at = moment().format()
  db.SetResidents(data)
})

ipcMain.on('export_database', (e) => {
  dialog.showSaveDialog(dialogOption, (filename) => {
    db.ExportDB(filename)
  })
})
