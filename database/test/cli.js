const sql = require("sql.js")
const fs = require("fs")
const readline = require("readline")

const dbPath = 'data/nursing_support_db.sqlite'

let filebuffer = fs.readFileSync(dbPath)
let db = new sql.Database(filebuffer)

let rl = readline.createInterface(process.stdin, process.stdout)
function standby() {
  rl.question("> ", (query) => {
    try {
      let result = db.exec(query)
      console.log(result[0].columns)
      for (let item of result[0].values) {
        console.log(item)
      }
      standby()
    } catch (err) {
      console.log(err)
      standby()
    }
  })
}

process.on('exit', function() {
  let data = db.export()
  let buffer = new Buffer(data)
  console.log('\nExitting...')
  fs.writeFileSync(dbPath, buffer)
})

process.on('SIGINT', function() {
  process.exit(0)
})

standby()
