// init
const fs = require("fs")
try {
  fs.statSync('data/nursing_support_db.sqlite')
  fs.unlinkSync('data/nursing_support_db.sqlite')
} catch (err) {}

const DBMethods = require("../db_module.js")
const db = new DBMethods()

// Residents
let residents = [
  {
    id: 1,
    name: "鈴木 一郎",
    name_ruby: "スズキ イチロウ",
    sex: "male",
    birthday: "2018-08-31T13:46:42+09:0",
    created_at: "2018-08-31T13:46:42+09:0",
    joined_at: "2018-08-31T13:46:42+09:0",
    room_number: 1
  },{
    id: 2,
    name: "山本 次郎",
    name_ruby: "ヤマモト ジロウ",
    sex: "male",
    birthday: "2018-08-31T13:46:42+09:0",
    created_at: "2018-08-31T13:46:42+09:0",
    joined_at: "2018-08-31T13:46:42+09:0",
    room_number: 1
  },{
    id: 3,
    name: "谷口 花子",
    name_ruby: "タニグチ ハナコ",
    sex: "female",
    birthday: "2018-09-01T13:46:42+09:0",
    created_at: "2018-09-01T13:46:42+09:0",
    joined_at: "2018-09-01T13:46:42+09:0",
    room_number: 2
  },{
    id: 4,
    name: "坂田 真里",
    name_ruby: "サカタ マリ",
    sex: "female",
    birthday: "2018-09-01T13:46:42+09:0",
    created_at: "2018-09-01T13:46:42+09:0",
    joined_at: "2018-09-01T13:46:42+09:0",
    room_number: 3
  }
]

let rooms = [
  {
    room_number: 1,
    seating_capacity: 2,
    created_at: "2018-09-01T13:46:42+09:0",
    joined_at: "2018-09-01T13:46:42+09:0",
  },{
    room_number: 2,
    seating_capacity: 1,
    created_at: "2018-09-01T13:46:42+09:0",
    joined_at: "2018-09-01T13:46:42+09:0",
  },{
    room_number: 3,
    seating_capacity: 4,
    created_at: "2018-09-01T13:46:42+09:0",
    joined_at: "2018-09-01T13:46:42+09:0",
  }
]

db.SetResidents(residents)
db.SetRooms(rooms)
