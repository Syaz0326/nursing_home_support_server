const DBMethods = require("../db_module.js")

const db = new DBMethods()

// insert test
/* Residents
db.SetResidents({
  id: 'ureoifjdlksvcx',
  name: 'ほげ',
  name_ruby: 'ほげ',
  sex: 'male',
  birthday: '1999-03-26',
  joined_at: '1999-03-26',
  created_at: '1999-03-26',
  updated_at: '1999-03-26',
  room_number: 1
})

db.SetResidents([
  {
    id: 'fdjlksa;fjbf',
    name: 'ふが',
    name_ruby: 'ふが',
    sex: 'male',
    birthday: '1999-03-26',
    joined_at: '1999-03-26',
    created_at: '1999-03-26',
    updated_at: '1999-03-26',
    room_number: 1
  },{
    id: 'biuokfwaends',
    name: 'ぴよ',
    name_ruby: 'ぴよ',
    sex: 'female',
    birthday: '1999-03-26',
    joined_at: '1999-03-26',
    created_at: '1999-03-26',
    updated_at: '1999-03-26',
    room_number: 2
  }
])
//*/

/*/ rooms
db.SetRooms({
  room_number: 1,
  seating_capacity: 4,
  created_at: '1900-01-01',
  updated_at: '1900-01-01'
})

db.SetRooms([
  {
    room_number: 2,
    seating_capacity: 1,
    created_at: '1900-01-01',
    updated_at: '1900-01-01'
  },{
    room_number: 3,
    seating_capacity: 2,
    created_at: '1900-01-01',
    updated_at: '1900-01-01'
  }
])
//*/

/*/ BloodPressure
db.SetBloodPressure({
  id: 'ureoifjdlksvcx',
  top: 140,
  bottom: 60,
  created_at: '1999-03-26',
  updated_at: '1999-03-26',
  resident_id: 'fjdlajfdal'
})
//*/

/*/ BodyTemperature
db.SetBodyTemperature({
  id: 'ureoifjdlksvcx',
  temperature: 36.4,
  created_at: '1999-03-26',
  updated_at: '1999-03-26',
  resident_id: 'fjdlajfdal'
})
//*/

/*/ Meal
db.SetMeal({
  id: 'ureoifjdlksvcx',
  staple_food: 4,
  side_dish: 6,
  created_at: '1999-03-26',
  updated_at: '1999-03-26',
  resident_id: 'fjdlajfdal'
})
//*/

/*/ Hydrate
db.SetHydrate({
  id: 'ureoifjdlksvcx',
  kind: 'お茶',
  intake: 100,
  created_at: '1999-03-26',
  updated_at: '1999-03-26',
  resident_id: 'fjdlajfdal'
})
//*/

/*/ Defecation
db.SetDefecation({
  id: 'ureoifjdlksvcx',
  volume: '大',
  color: '淡黄',
  form: 'コロコロ',
  created_at: '1999-03-26',
  updated_at: '1999-03-26',
  resident_id: 'fjdlajfdal'
})
//*/

/*/ Dosage
db.SetDosage({
  id: 'ureoifjdlksvcx',
  kind: 'ラキソベロン',
  dosage: 20,
  created_at: '1999-03-26',
  updated_at: '1999-03-26',
  resident_id: 'fjdlajfdal'
})
//*/

/*/ Urinate
db.SetUrinate({
  id: 'ureoifjdlksvcx',
  volume: 100,
  created_at: '1999-03-26',
  updated_at: '1999-03-26',
  resident_id: 'fjdlajfdal'
})
//*/

// select test
let result = null
/* residents
result = db.GetResidents()
console.log(result)
//*/

/* rooms
result = db.GetRooms()
console.log(result)
//*/

/* BloodPressure
result = db.GetBloodPressure('qawsedrftgyhujikolp')
console.log(result)
//*/

/*/ BodyTemperature
result = db.GetBodyTemperature('ureoifjdlksvcx')
console.log(result)
//*/

/*/ Meal
result = db.GetMeal('ureoifjdlksvcx')
console.log(result)
//*/

/*/ Hydrate
result = db.GetHydrate('ureoifjdlksvcx')
console.log(result)
//*/

/*/ Defecation
result = db.GetDefecation('ureoifjdlksvcx')
console.log(result)
//*/

/*/ Dosage
result = db.GetDosage('ureoifjdlksvcx')
console.log(result)
//*/

/*/ Urinate
result = db.GetUrinate('ureoifjdlksvcx')
console.log(result)
//*/

// result = db.GetResidentData(1)
// console.log(result)
