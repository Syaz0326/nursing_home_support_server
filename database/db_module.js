'use strict'
const sql = require("sql.js")
const fs = require("fs")
const uuid = require("uuid")
const uuidv4 = uuid.v4

const dbPath = 'data/nursing_support_db.sqlite'
const backupDir = 'data/backup/'

// Constructor
let DBMethods = function() {
  this.db = null
  this.filebuffer = null
  this.PrepareDB()
}

// Common
DBMethods.prototype.InitDB = function() {
  let schema = fs.readFileSync('database/schema.sql', 'utf8')
  let buffer

  this.db = new sql.Database()
  this.db.run(schema) //テーブル定義
  buffer = new Buffer(this.db.export())
  fs.writeFileSync(dbPath, buffer)
  this.filebuffer = fs.readFileSync(dbPath)
}

DBMethods.prototype.LoadDB = function() {
  this.filebuffer = fs.readFileSync(dbPath)
  this.db = new sql.Database(this.filebuffer)
}

DBMethods.prototype.PrepareDB = function() {
  try {
    fs.statSync(dbPath)
    this.LoadDB()
  } catch (err) {
    this.InitDB()
  }
}

DBMethods.prototype.ExportDB = function(path = dbPath) {
  let data = this.db.export()
  let buffer = new Buffer(data)
  fs.writeFileSync(path, buffer)
}

// SELECT
DBMethods.prototype.GetResidents = function() {
  let sqlstr =
    `SELECT id, name, name_ruby, sex, room_number
    FROM residents`
  let result = []
  this.db.each(sqlstr, (row) => {
    result.push(row)
  })
  return result
}

DBMethods.prototype.GetRooms = function() {
  let sqlstr =
    `SELECT room_number, seating_capacity
    FROM rooms`
  let result = []
  this.db.each(sqlstr, (row) => {
    result.push(row)
  })
  return result
}

DBMethods.prototype.GetBloodPressure = function(id) {
  let sqlstr =
    `SELECT * FROM blood_pressure
    WHERE id=$id
    LIMIT 1;`
  let result

  this.db.each(sqlstr,
    {
      '$id': id
    }, (row) => {
      result = row
    }
  )

  return result
}

DBMethods.prototype.GetBodyTemperature = function(id) {
  let sqlstr =
    `SELECT * FROM body_temperature
    WHERE id=$id
    LIMIT 1;`
  let result

  this.db.each(sqlstr,
    {
      '$id': id
    }, (row) => {
      result = row
    }
  )

  return result
}

DBMethods.prototype.GetMeal = function(id) {
  let sqlstr =
    `SELECT * FROM meal
    WHERE id=$id
    LIMIT 1;`
  let result

  this.db.each(sqlstr,
    {
      '$id': id
    }, (row) => {
      result = row
    }
  )

  return result
}

DBMethods.prototype.GetHydrate = function(id) {
  let sqlstr =
    `SELECT * FROM hydrate
    WHERE id=$id
    LIMIT 1;`
  let result

  this.db.each(sqlstr,
    {
      '$id': id
    }, (row) => {
      result = row
    }
  )

  return result
}

DBMethods.prototype.GetDefecation = function(id) {
  let sqlstr =
    `SELECT * FROM defecation
    WHERE id=$id
    LIMIT 1;`
  let result

  this.db.each(sqlstr,
    {
      '$id': id
    }, (row) => {
      result = row
    }
  )

  return result
}

DBMethods.prototype.GetDosage = function(id) {
  let sqlstr =
    `SELECT * FROM dosage
    WHERE id=$id
    LIMIT 1;`
  let result

  this.db.each(sqlstr,
    {
      '$id': id
    }, (row) => {
      result = row
    }
  )

  return result
}

DBMethods.prototype.GetUrinate = function(id) {
  let sqlstr =
    `SELECT * FROM urinate
    WHERE id=$id
    LIMIT 1;`
  let result

  this.db.each(sqlstr,
    {
      '$id': id
    }, (row) => {
      result = row
    }
  )

  return result
}

DBMethods.prototype.GetResidentBloodPressure = function(resident_id) {
  let sqlstr =
    `SELECT id, created_at, updated_at FROM blood_pressure
    WHERE resident_id=$resident_id;`
  let result = []

  this.db.each(sqlstr,
    {
      '$resident_id': resident_id
    }, (row) => {
      result.push(Object.assign(
        { type: 'blood_pressure' },
        row
      ))
    }
  )

  return result
}

DBMethods.prototype.GetResidentBodyTemperature = function(resident_id) {
  let sqlstr =
    `SELECT id, created_at, updated_at FROM body_temperature
    WHERE resident_id=$resident_id;`
  let result = []

  this.db.each(sqlstr,
    {
      '$resident_id': resident_id
    }, (row) => {
      result.push(Object.assign(
        { type: 'body_temperature' },
        row
      ))
    }
  )

  return result
}

DBMethods.prototype.GetResidentMeal = function(resident_id) {
  let sqlstr =
    `SELECT id, created_at, updated_at FROM meal
    WHERE resident_id=$resident_id;`
  let result = []

  this.db.each(sqlstr,
    {
      '$resident_id': resident_id
    }, (row) => {
      result.push(Object.assign(
        { type: 'meal' },
        row
      ))
    }
  )

  return result
}

DBMethods.prototype.GetResidentHydrate = function(resident_id) {
  let sqlstr =
    `SELECT id, created_at, updated_at FROM hydrate
    WHERE resident_id=$resident_id;`
  let result = []

  this.db.each(sqlstr,
    {
      '$resident_id': resident_id
    }, (row) => {
      result.push(Object.assign(
        { type: 'hydrate' },
        row
      ))
    }
  )

  return result
}

DBMethods.prototype.GetResidentDefecation = function(resident_id) {
  let sqlstr =
    `SELECT id, created_at, updated_at FROM defecation
    WHERE resident_id=$resident_id;`
  let result = []

  this.db.each(sqlstr,
    {
      '$resident_id': resident_id
    }, (row) => {
      result.push(Object.assign(
        { type: 'defecation' },
        row
      ))
    }
  )

  return result
}

DBMethods.prototype.GetResidentDosage = function(resident_id) {
  let sqlstr =
    `SELECT id, created_at, updated_at FROM dosage
    WHERE resident_id=$resident_id;`
  let result = []

  this.db.each(sqlstr,
    {
      '$resident_id': resident_id
    }, (row) => {
      result.push(Object.assign(
        { type: 'dosage' },
        row
      ))
    }
  )

  return result
}

DBMethods.prototype.GetResidentUrinate = function(resident_id) {
  let sqlstr =
    `SELECT id, created_at, updated_at FROM urinate
    WHERE resident_id=$resident_id;`
  let result = []

  this.db.each(sqlstr,
    {
      '$resident_id': resident_id
    }, (row) => {
      result.push(Object.assign(
        { type: 'urinate' },
        row
      ))
    }
  )

  return result
}

DBMethods.prototype.GetResidentData = function(resident_id) {
  let result = []

  Array.prototype.push.apply(result, this.GetResidentBloodPressure(resident_id))
  Array.prototype.push.apply(result, this.GetResidentBodyTemperature(resident_id))
  Array.prototype.push.apply(result, this.GetResidentMeal(resident_id))
  Array.prototype.push.apply(result, this.GetResidentHydrate(resident_id))
  Array.prototype.push.apply(result, this.GetResidentDefecation(resident_id))
  Array.prototype.push.apply(result, this.GetResidentDosage(resident_id))
  Array.prototype.push.apply(result, this.GetResidentUrinate(resident_id))
  return result
}

// INSERT
DBMethods.prototype.SetResidents = function(residents) {
  let sqlstr =
    `INSERT INTO residents(
      id,
      name,
      name_ruby,
      sex,
      birthday,
      joined_at,
      created_at,
      room_number
    )
    VALUES (
    $id,
    $name,
    $name_ruby,
    $sex,
    $birthday,
    $joined_at,
    $created_at,
    $room_number
    );`

  // 引数が配列でなければ配列に変更
  if (!Array.isArray(residents)) {
    residents = [residents]
  }

  residents.forEach((resident, index) => {
    this.db.each(sqlstr,
      {
        '$id': uuidv4(),
        '$name': resident.name,
        '$name_ruby': resident.name_ruby,
        '$sex': resident.sex,
        '$birthday': resident.birthday,
        '$joined_at': resident.joined_at,
        '$created_at': resident.created_at,
        '$updated_at': resident.updated_at,
        '$room_number': resident.room_number
      }
    )
    if (index === residents.length - 1) {
      this.ExportDB()
    }
  })
}

DBMethods.prototype.SetRooms = function(rooms) {
  let sqlstr =
    `INSERT INTO rooms
    VALUES (
    $room_number,
    $seating_capacity,
    $created_at,
    $updated_at
    );`

  if(!Array.isArray(rooms)) {
    rooms = [rooms]
  }

  rooms.forEach((room, index) => {
    this.db.each(sqlstr,
      {
        '$room_number': room.room_number,
        '$seating_capacity': room.seating_capacity,
        '$created_at': room.created_at,
        '$updated_at': room.updated_at
      }
    )

    if (index === rooms.length - 1) {
      this.ExportDB()
    }
  })
}

DBMethods.prototype.SetBloodPressure = function(data) {
  let sqlstr =
    `INSERT INTO blood_pressure
    VALUES (
    $id,
    $top,
    $bottom,
    $created_at,
    $updated_at,
    $resident_id
    );`

  this.db.each(sqlstr,
    {
      '$id': data.id,
      '$top': data.top,
      '$bottom': data.bottom,
      '$created_at': data.created_at,
      '$updated_at': data.updated_at,
      '$resident_id': data.resident_id
    },() => {},
    () => {
      this.ExportDB()
    }
  )
}

DBMethods.prototype.SetBodyTemperature = function(data) {
  let sqlstr =
    `INSERT INTO body_temperature
    VALUES (
    $id,
    $temperature,
    $created_at,
    $updated_at,
    $resident_id
    );`

  this.db.each(sqlstr,
    {
      '$id': data.id,
      '$temperature': data.temperature,
      '$created_at': data.created_at,
      '$updated_at': data.updated_at,
      '$resident_id': data.resident_id
    },() => {},
    () => {
      this.ExportDB()
    }
  )
}

DBMethods.prototype.SetMeal = function(data) {
  let sqlstr =
    `INSERT INTO meal
    VALUES (
      $id,
      $staple_food,
      $side_dish,
      $created_at,
      $updated_at,
      $resident_id
    );`

  this.db.each(sqlstr,
    {
      '$id': data.id,
      '$staple_food': data.staple_food,
      '$side_dish': data.side_dish,
      '$created_at': data.created_at,
      '$updated_at': data.updated_at,
      '$resident_id': data.resident_id
    }, () => {},
    () => {
      this.ExportDB()
    }
  )
}

DBMethods.prototype.SetHydrate = function(data) {
  let sqlstr =
    `INSERT INTO hydrate
    VALUES (
      $id,
      $kind,
      $intake,
      $created_at,
      $updated_at,
      $resident_id
    );`

  this.db.each(sqlstr,
    {
      '$id': data.id,
      '$kind': data.kind,
      '$intake': data.intake,
      '$created_at': data.created_at,
      '$updated_at': data.updated_at,
      '$resident_id': data.resident_id
    }, () => {},
    () => {
      this.ExportDB()
    }
  )
}

DBMethods.prototype.SetDefecation = function(data) {
  let sqlstr =
    `INSERT INTO defecation
    VALUES (
      $id,
      $volume,
      $color,
      $form,
      $created_at,
      $updated_at,
      $resident_id
    );`

  this.db.each(sqlstr,
    {
      '$id': data.id,
      '$volume': data.volume,
      '$color': data.color,
      '$form': data.form,
      '$created_at': data.created_at,
      '$updated_at': data.updated_at,
      '$resident_id': data.resident_id
    }, () => {},
    () => {
      this.ExportDB()
    }
  )
}

DBMethods.prototype.SetDosage = function(data) {
  let sqlstr =
    `INSERT INTO dosage
    VALUES (
      $id,
      $kind,
      $dose,
      $created_at,
      $updated_at,
      $resident_id
    );`

  this.db.each(sqlstr,
    {
      '$id': data.id,
      '$kind': data.kind,
      '$dose': data.dose,
      '$created_at': data.created_at,
      '$updated_at': data.updated_at,
      '$resident_id': data.resident_id
    }, () => {},
    () => {
      this.ExportDB()
    }
  )
}

DBMethods.prototype.SetUrinate = function(data) {
  let sqlstr =
    `INSERT INTO urinate
    VALUES (
      $id,
      $volume,
      $created_at,
      $updated_at,
      $resident_id
    );`

  this.db.each(sqlstr,
    {
      '$id': data.id,
      '$volume': data.volume,
      '$created_at': data.created_at,
      '$updated_at': data.updated_at,
      '$resident_id': data.resident_id
    }, () => {},
    () => {
      this.ExportDB()
    }
  )
}

// UPDATE
DBMethods.prototype.InitUpdate = function(data) {
  let sqlstr = ''
  let param = {}
  let id = data.id
  delete data.id
  for (var key of Object.keys(data)) {
    sqlstr += (`${key} = $${key},`)
    param['$' + key] = data[key]
  }
  sqlstr = sqlstr.slice(0, -1)
  param['$id'] = id
  return {
    sqlstr: sqlstr + '\n  WHERE id = $id;',
    param: param
  }
}

DBMethods.prototype.UpdateBloodPressure = function(data) {
  let sqlstr =
  `UPDATE blood_pressure
  SET `
  let param = {}
  let initializedData = this.InitUpdate(data)
  sqlstr += initializedData.sqlstr
  param = initializedData.param

  this.db.each(sqlstr, param, () => {},
  () => {
    this.ExportDB()
  })
}

DBMethods.prototype.UpdateBodyTemperature = function(data) {
  let sqlstr =
  `UPDATE body_temperature
  SET `
  let param = {}
  let initializedData = this.InitUpdate(data)
  sqlstr += initializedData.sqlstr
  param = initializedData.param

  this.db.each(sqlstr, param, () => {},
  () => {
    this.ExportDB()
  })
}

DBMethods.prototype.UpdateMeal = function(data) {
  let sqlstr =
  `UPDATE meal
  SET `
  let param = {}
  let initializedData = this.InitUpdate(data)
  sqlstr += initializedData.sqlstr
  param = initializedData.param

  this.db.each(sqlstr, param, () => {},
  () => {
    this.ExportDB()
  })
}

DBMethods.prototype.UpdateHydrate = function(data) {
  let sqlstr =
  `UPDATE hydrate
  SET `
  let param = {}
  let initializedData = this.InitUpdate(data)
  sqlstr += initializedData.sqlstr
  param = initializedData.param

  this.db.each(sqlstr, param, () => {},
  () => {
    this.ExportDB()
  })
}

DBMethods.prototype.UpdateDefecation = function(data) {
  let sqlstr =
  `UPDATE defecation
  SET `
  let param = {}
  let initializedData = this.InitUpdate(data)
  sqlstr += initializedData.sqlstr
  param = initializedData.param

  this.db.each(sqlstr, param, () => {},
  () => {
    this.ExportDB()
  })
}

DBMethods.prototype.UpdateDosage = function(data) {
  let sqlstr =
  `UPDATE dosage
  SET `
  let param = {}
  let initializedData = this.InitUpdate(data)
  sqlstr += initializedData.sqlstr
  param = initializedData.param

  this.db.each(sqlstr, param, () => {},
  () => {
    this.ExportDB()
  })
}

DBMethods.prototype.UpdateUrinate = function(data) {
  let sqlstr =
  `UPDATE urinate
  SET `
  let param = {}
  let initializedData = this.InitUpdate(data)
  sqlstr += initializedData.sqlstr
  param = initializedData.param

  this.db.each(sqlstr, param, () => {},
  () => {
    this.ExportDB()
  })
}

module.exports = DBMethods
