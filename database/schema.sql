CREATE TABLE residents (
  id VARCHAR(64) PRIMARY KEY,
  name VARCHAR(64) NOT NULL,
  name_ruby VARCHAR(64) NOT NULL,
  sex VARCHAR(64) NOT NULL
  CHECK (sex = 'male' OR sex = 'female'),
  birthday VARCHAR(64) NOT NULL,
  joined_at VARCHAR(64) NOT NULL,
  created_at VARCHAR(64) NOT NULL,
  updated_at VARCHAR(64),
  room_number INT NOT NULL
);

CREATE TABLE blood_pressure (
  id VARCHAR(64) PRIMARY KEY,
  top INT NOT NULL,
  bottom INT NOT NULL,
  created_at VARCHAR(64) NOT NULL,
  updated_at VARCHAR(64),
  resident_id VARCHAR(64) NOT NULL
);

CREATE TABLE body_temperature (
  id VARCHAR(64) PRIMARY KEY,
  temperature REAL NOT NULL,
  created_at VARCHAR(64) NOT NULL,
  updated_at VARCHAR(64),
  resident_id VARCHAR(64) NOT NULL
);

CREATE TABLE meal (
  id VARCHAR(64) PRIMARY KEY,
  staple_food INT NOT NULL,
  side_dish INT NOT NULL,
  created_at VARCHAR(64) NOT NULL,
  updated_at VARCHAR(64),
  resident_id VARCHAR(64) NOT NULL
);

CREATE TABLE hydrate (
  id VARCHAR(64) PRIMARY KEY,
  kind VARCHAR(64) NOT NULL,
  -- CHECK (kind = 'ヤクルト' OR kind = 'お茶' OR kind = '水' OR kind = '点滴' OR kind = 'ポカリ'),
  intake INT NOT NULL,
  created_at VARCHAR(64) NOT NULL,
  updated_at VARCHAR(64),
  resident_id VARCHAR(64) NOT NULL
);

CREATE TABLE defecation (
  id VARCHAR(64) PRIMARY KEY,
  volume VARCHAR(64) NOT NULL,
  -- CHECK (volume = '極小' OR volume = '小' OR volume = '中' OR volume = '大'),
  color VARCHAR(64) NOT NULL,
  -- CHECK (color = '淡黄' OR color = '黄褐' OR color = '茶' OR color = '褐色' OR color = '灰' OR color = '黒'),
  form VARCHAR(64) NOT NULL,
  -- CHECK (form = 'コロコロ' OR form = 'バナナ' OR form = '泥状'),
  created_at VARCHAR(64) NOT NULL,
  updated_at VARCHAR(64),
  resident_id VARCHAR(64) NOT NULL
);

CREATE TABLE dosage (
  id VARCHAR(64) PRIMARY KEY,
  kind VARCHAR(64) NOT NULL,
  -- CHECK (kind = 'ラキソベロン' OR kind = 'プルゼニド'),
  dose INT NOT NULL,
  created_at VARCHAR(64) NOT NULL,
  updated_at VARCHAR(64),
  resident_id VARCHAR(64) NOT NULL
);

CREATE TABLE urinate (
  id VARCHAR(64) PRIMARY KEY,
  volume INT NOT NULL,
  created_at VARCHAR(64) NOT NULL,
  updated_at VARCHAR(64),
  resident_id VARCHAR(64) NOT NULL
);

CREATE TABLE rooms (
  room_number INT PRIMARY KEY,
  seating_capacity INT NOT NULL,
  created_at VARCHAR(64) NOT NULL,
  updated_at VARCHAR(64)
);
