`use strict`

let Store = function() {
}

Store.prototype = {
  db: null,
  server: null,
  setDB(db) {
    Store.prototype.db = db
  },
  setServer(server) {
    Store.prototype.server = server
  },
  test() {
    console.log('test')
    console.log(Store.prototype.db)
    console.log(Store.prototype.server)
  }
}

module.exports = Store
