let state = {
  isRunning: false,
  HOST: null,
  PORT: null,
  serverMessage: 'server is stopped'
}

let getters = {
  getStateSentence: (state) => {
    if(state.isRunning) {
      return "サーバは起動しています"
    } else {
      return "サーバは停止しています"
    }
  }
}

let actions = {
  setStartServer({ commit }, status) {
    commit("updateIsRunning", true)
    commit("setHOST", status.HOST)
    commit("setPORT", status.PORT)
    commit("updateServerMessage", `server is listening to ${status.HOST}:${status.PORT}`)
  },
  setStopServer({ commit }, status) {
    commit("updateIsRunning", false)
    commit("setHOST", '')
    commit("setPORT", '')
    commit("updateServerMessage", 'server is stopped')
  }
}

let mutations = {
  updateIsRunning: (state, isRunning) => state.isRunning = isRunning,
  updateServerMessage: (state, message) => state.serverMessage = message,
  setHOST: (state, HOST) => state.HOST = HOST,
  setPORT: (state, PORT) => state.PORT = PORT
}

module.exports = {
  state: state,
  getters: getters,
  actions: actions,
  mutations: mutations
}
