// import components
import Top from './app/top'
import OperationDatabase from './app/operation_database'
import ConnectDevice from './app/connect_device'
import Registration from './app/registration'

export default [
  {
    path: '/',
    component: Top
  },{
    path: '/operation-database',
    component: OperationDatabase,
  },{
    path: '/operation-database/registration',
    component: Registration
  },{
    path: '/connect-device',
    component: ConnectDevice
  }
]
