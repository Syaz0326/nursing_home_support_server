const ip = require("ip")

let getIp  = () => {
  return ip.address()
}

module.exports = getIp
