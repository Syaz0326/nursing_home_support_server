// dosage parameter
let medicineList = [
  {
    id: 'laxoveron',
    name: 'ラキソベロン',
    param: 1
  },{
    id: 'pursennid',
    name: "プルゼニド",
    param: 7.5
  }
]
let params = {
  pursennid: 7.5
}

let ConvertUnit = {
  // 滴薬換算のデータに変更する
  convertDosage(data) {
    let medicine = medicineList.find((item) => {
      return item.name === data.kind
    })
    return data.dose * medicine.param
  },
  inverseDosage(data) {
    let medicine = medicineList.find((item) => {
      return item.name === data.kind
    })
    return data.dose / medicine.param
  }
}

module.exports = ConvertUnit
