// server require
const express = require("express")
const app = express()
const bodyParser = require("body-parser")

const Store = require("../store/store.js")
let store = new Store()

const DBMethods = require("../database/db_module.js")
store.setDB(new DBMethods())
const db = store.db

const ConvertUnit = require("./convert_unit.js")

// const HOST = "localhost"
// const PORT = 10080

// cors
app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*")
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
  res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE")
  next()
})

// POST method settings
app.use(bodyParser.urlencoded({
  extended: true
}))
app.use(bodyParser.json())

// GET
app.get('/get-all-residents', (req, res) => {
  let result = db.GetResidents()
  res.send(result)
})

app.get('/get-all-rooms', (req, res) => {
  let result = db.GetRooms()
  res.send(result)
})

app.get('/get-resident-data', (req, res) => {
  let resident_id = req.query.resident_id
  let result = db.GetResidentData(resident_id)
  res.send(result)
})

app.get('/get-blood-pressure', (req, res) => {
  let id = req.query.id
  let result = db.GetBloodPressure(id)
  res.send(result)
})

app.get('/get-body-temperature', (req, res) => {
  let id = req.query.id
  let result = db.GetBodyTemperature(id)
  res.send(result)
})

app.get('/get-meal', (req, res) => {
  let id = req.query.id
  let result = db.GetMeal(id)
  res.send(result)
})

app.get('/get-hydrate', (req, res) => {
  let id = req.query.id
  let result = db.GetHydrate(id)
  res.send(result)
})

app.get('/get-defecation', (req, res) => {
  let id = req.query.id
  let result = db.GetDefecation(id)
  res.send(result)
})

app.get('/get-dosage', (req, res) => {
  let id = req.query.id
  let result = db.GetDosage(id)
  result.dose = ConvertUnit.inverseDosage({
    kind: result.kind,
    dose: result.dose
  })
  res.send(result)
})

app.get('/get-urinate', (req, res) => {
  let id = req.query.id
  let result = db.GetUrinate(id)
  res.send(result)
})

// POST
app.post('/post-blood-pressure', (req, res) => {
  let data = req.body
  db.SetBloodPressure({
    id: data.id,
    top: data.top,
    bottom: data.bottom,
    created_at: data.created_at,
    updated_at: data.updated_at ? data.updated_at : null,
    resident_id: data.resident_id
  })
  res.sendStatus(201)
})

app.post('/post-body-temperature', (req, res) => {
  let data = req.body
  db.SetBodyTemperature({
    id: data.id,
    temperature: data.temperature,
    created_at: data.created_at,
    updated_at: data.updated_at ? data.updated_at : null,
    resident_id: data.resident_id
  })
  res.sendStatus(201)
})

app.post('/post-meal', (req, res) => {
  let data = req.body
  db.SetMeal({
    id: data.id,
    staple_food: data.staple_food,
    side_dish: data.side_dish,
    created_at: data.created_at,
    updated_at: data.updated_at ? data.updated_at : null,
    resident_id: data.resident_id
  })
  res.sendStatus(201)
})

app.post('/post-hydrate', (req, res) => {
  let data = req.body
  db.SetHydrate({
    id: data.id,
    kind: data.kind,
    intake: data.intake,
    created_at: data.created_at,
    updated_at: data.updated_at ? data.updated_at : null,
    resident_id: data.resident_id
  })
  res.sendStatus(201)
})

app.post('/post-defecation', (req, res) => {
  let data = req.body
  db.SetDefecation({
    id: data.id,
    volume: data.volume,
    color: data.color,
    form: data.form,
    created_at: data.created_at,
    updated_at: data.updated_at ? data.updated_at : null,
    resident_id: data.resident_id
  })
  res.sendStatus(201)
})

app.post('/post-dosage', (req, res) => {
  let data = req.body
  let convertedDosage = ConvertUnit.convertDosage({
    kind: data.kind,
    dose: data.dose
  })
  db.SetDosage({
    id: data.id,
    kind: data.kind,
    dose: convertedDosage,
    created_at: data.created_at,
    updated_at: data.updated_at ? data.updated_at : null,
    resident_id: data.resident_id
  })
  res.sendStatus(201)
})

app.post('/post-urinate', (req, res) => {
  let data = req.body
  db.SetUrinate({
    id: data.id,
    volume: data.volume,
    created_at: data.created_at,
    updated_at: data.updated_at ? data.updated_at : null,
    resident_id: data.resident_id
  })
  res.sendStatus(201)
})

app.put('/put-blood-pressure', (req, res) => {
  let data = req.body
  db.UpdateBloodPressure(data)
  res.sendStatus(201)
})

app.put('/put-body-temperature', (req, res) => {
  let data = req.body
  db.UpdateBodyTemperature(data)
  res.sendStatus(201)
})

app.put('/put-meal', (req, res) => {
  let data = req.body
  db.UpdateMeal(data)
  res.sendStatus(201)
})

app.put('/put-hydrate', (req, res) => {
  let data = req.body
  db.UpdateHydrate(data)
  res.sendStatus(201)
})

app.put('/put-defecation', (req, res) => {
  let data = req.body
  db.UpdateDefecation(data)
  res.sendStatus(201)
})

app.put('/put-dosage', (req, res) => {
  let data = req.body
  data.dose = ConvertUnit.convertDosage({
    kind: data.kind,
    dose: data.dose
  })
  db.UpdateDosage(data)
  res.sendStatus(201)
})

app.put('/put-urinate', (req, res) => {
  let data = req.body
  db.UpdateUrinate(data)
  res.sendStatus(201)
})

// サーバ操作メソッド
let ServerMethods = function() {
  this.server = null
}

ServerMethods.prototype.start = function(HOST = 'localhost', PORT = 10080) {
  this.server = app.listen(PORT, () => {
  })
}

module.exports = ServerMethods
